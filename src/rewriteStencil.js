const replace = require('replace-in-file');

const pathsToStencilCompiler = [
    './node_modules/@stencil/core/dist/compiler/index.js',
    './node_modules/@stencil/core/compiler/index.js',
    './node_modules/@stencil/core/compiler/stencil.js',
];

const doReplaces = async path => {
    try {
        const res1 = await replace({
            files: path,
            from: "const STYLE_EXT = ['css', 'scss', 'sass', 'pcss', 'styl', 'stylus', 'less']",
            to: "const STYLE_EXT = ['css', 'scss', 'sass', 'pcss', 'styl', 'stylus', 'less', 'ts'];"
        })
        const res2 = await replace({
            files: path,
            from: "if (!styleText.includes('@import')) {",
            to: "if (!styleText.includes('import')) {"
        })
        const res3 = await replace({
            files: path,
            from: "const IMPORT_RE = /(@import)\\s+(url\\()?\\s?(.*?)\\s?\\)?([^;]*);?/gi;",
            to: "const IMPORT_RE = /(@import|import).*(url\\(|from)\\s?(.*?)\\s?\\)?([^;]*);?/gi;"
        })
        if (res1.length) console.log(`Rewrote file: ${res1[0]}`)
        if (res2.length) console.log(`Rewrote file: ${res2[0]}`)
        if (res3.length) console.log(`Rewrote file: ${res3[0]}`)
    } catch (e) {
    }
}


(() => {
    pathsToStencilCompiler.forEach(doReplaces)
})()
