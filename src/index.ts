import { loadDiagnostic } from './diagnostics';
import sucrase from '@rollup/plugin-sucrase';
import { rollup } from 'rollup';
import { default as stylis } from 'stylis';
import * as d from './declarations';
import validator from 'csstree-validator';
import cssBeautify from 'cssbeautify';
import chalk from 'chalk';
import commonjs from '@rollup/plugin-commonjs';
import nodeResolve from '@rollup/plugin-node-resolve';
import glob from 'glob';
import path from 'path';
import rimraf from 'rimraf';
import { promisify } from 'util';

interface IConfig {
  logCssErrors?: boolean;
  tempFolder?: string;
  rootDir?: string;
  stylesGlob?: string;
  outputGenerator?: (
    fileName: string,
    results: d.PluginTransformResults,
    context?: any
  ) => Promise<any>;
}

export function tss(config?: IConfig) {
  const { logCssErrors, tempFolder, rootDir, stylesGlob, outputGenerator } = {
    logCssErrors: true,
    tempFolder: './node_modules/.tssTemp',
    rootDir: process.cwd(),
    stylesGlob: '**/*.styles.ts',
    outputGenerator: async (fileName, results, context) => {
      if (context) {
        return context.fs
          .writeFile(results.id, results.code, {
            inMemoryOnly: true
          })
          .catch((err: any) => {
            console.error(err);
            loadDiagnostic(context, err, fileName);
            results.code = `/**  ts-style error${
              err && err.message ? ': ' + err.message : ''
            }  **/`;
            return results;
          });
      }
      return Promise.resolve();
    },
    ...config
  } as IConfig;
  const globP = promisify(glob);
  const rimrafP = promisify(rimraf);

  const tempPath = path.isAbsolute(tempFolder)
    ? tempFolder
    : path.resolve(tempFolder);
  const filesGlobP = globP(stylesGlob, { cwd: rootDir });
  const stylesBundlePromise = (async () => {
    await rimrafP(tempPath);
    return (await rollup({
      input: (await filesGlobP).map(filepath => path.join(rootDir, filepath)),
      onwarn: () => {
        /* empty */
      },
      preserveSymlinks: true,
      plugins: [
        sucrase({ transforms: ['typescript'] }),
        nodeResolve({ extensions: ['.js', '.ts'] }),
        commonjs({ extensions: ['.js', '.ts'] })
      ]
    })).write({
      dir: tempPath,
      format: 'cjs'
    });
  })();

  const usePlugin = async (fileName: string) =>
    (await filesGlobP).includes(
      path.relative(rootDir, fileName).replace(/\\/g, '/')
    );
  const changeFileNameExt = (fileName: string, ext: string) =>
    fileName
      .split('.')
      .map((part, i, arr) => (i === arr.length - 1 ? ext : part))
      .join('.');

  return {
    name: 'tss',
    pluginType: 'css',
    transform: async (
      sourceText: string,
      fileName: string,
      context: d.PluginCtx
    ) => {
      if (!context || !(await usePlugin(fileName))) {
        return null;
      }
      const results: d.PluginTransformResults = {
        id: changeFileNameExt(fileName, 'css'),
        code: ''
      };
      if (sourceText.trim() === '') {
        return Promise.resolve(results);
      }

      await stylesBundlePromise.catch(err => {
        console.error(err);
        loadDiagnostic(context, err, fileName);
        results.code = `/**  ts-style error${
          err && err.message ? ': ' + err.message : ''
        }  **/`;
        return results;
      });
      const jsFileName = changeFileNameExt(fileName, 'js');
      const pathToStyleTemp = path.resolve(
        `${tempPath}/${path.basename(jsFileName, '.js')}`
      );
      const style = require(pathToStyleTemp);
      results.code = results.code + stylis('', style);
      if (logCssErrors) {
        JSON.parse(
          validator.reporters.json(validator.validateString(results.code))
        ).forEach((error: any) => {
          if (
            !['-webkit-', '-ms-', '-moz-'].some(prefix =>
              error.message.includes(prefix)
            )
          ) {
            console.log(
              chalk.red('CSS-Error in file:') +
                fileName.substring(fileName.lastIndexOf('/'))
            );
            console.log(chalk.red(error.message));
            if (error.details) {
              console.log('\n' + chalk.cyan('Details:'));
              console.log(error.details);
            }
            console.log('\n' + chalk.cyan('Exactly here:'));
            console.log(
              `${results.code.substr(error.column - 11, 10)}${chalk.red(
                results.code[error.column - 1]
              )}${results.code.substr(error.column, 10)}`
            );
            console.log('\n' + chalk.cyan('Around here:'));
            const cssBefore = results.code.substring(0, error.column);
            const cssAfter = results.code.substring(error.column);
            console.log(
              cssBeautify(
                results.code.substring(
                  cssBefore.lastIndexOf('}') - 15,
                  cssBefore.length + cssAfter.indexOf('}') + 15
                )
              )
            );
            console.log('\n' + '-'.repeat(35) + '\n');
          }
        });
      }
      // write this css content to memory only so it can be referenced
      // later by other plugins (autoprefixer)
      // but no need to actually write to disk

      await outputGenerator(fileName, results, context);
      return results;
    },
    generateBundle: () => {
      rimraf(tempPath, () => {
        /* empty */
      });
    }
  };
}
