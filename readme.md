# stencil-plugin-tss

### Repo based on @stencil/sass

This package is used to transpile `.ts` files to css styles. It is optimized for a Stencil build, but may work in other environments too.

First, npm install within the project:

```
npm install stencil-plugin-tss --save-dev
```

Next, within the project's `rollup.config.js` or `stencil.config.js` file, import the plugin and add it to the config's `plugins` config:

#### stencil.config.ts

```ts
import { Config } from '@stencil/core';
import { tss } from 'stencil-plugin-tss';

export const config: Config = {
  plugins: [tss(options)]
};
```

The plugin will start transpiling `.ts` files as soon as it is initialized. The transpiled files will be read by the transform hook to generate css styles.

### Options

#### options.logCssErrors `boolean, default: true`

This will toggle the output of debugging information. There'll be hints displayed in the console if the compiled styles are faulty.

#### options.tempFolder `string, default: "./node_modules/.tssTemp"`

Specify a folder in which to output transpiled `.ts` files. There'll be cjs modules generated into this folder - this is
not the final output of the plugin and will be deleted in the `generateBundle` hook.

#### options.stylesGlob `string, default: "**/*.styles.ts"`

Pass a glob that resolves to every `.ts` file you want to use as styles.

#### options.rootDir `string, default: process.cwd()`

Will be used as root for globbing.

#### options.outputGenerator `function, default: output to stencil inMemory fs`

Pass a function to specify what to do with the transformation results.
Function signature: `(fileName: string, results: d.PluginTransformResults, context?: any) => Promise<any>`
Context will be the stencil context object passed to the transform hook.
Default function looks like this:

```
async (fileName, results, context) => {
  if (context) {
    return context.fs
      .writeFile(results.id, results.code, {
        inMemoryOnly: true
      })
      .catch((err: any) => {
        console.error(err);
        loadDiagnostic(context, err, fileName);
        results.code = `/**  ts-style error${
          err && err.message ? ': ' + err.message : ''
        }  **/`;
        return results;
      });
  }
  return Promise.resolve();
}
```

### Code Highlighting

For better usability install styled component plugin within your IDE:

- [VSCode](https://github.com/styled-components/vscode-styled-components)
- [IntelliJ](https://plugins.jetbrains.com/plugin/9997-styled-components)

You can then use:
`import { styled } from "stencil-plugin-tss"`

And prefix all your template literal CSS strings with `styled` to get CSS highlighting and IntelliSense.

### HMR

For better hot-module-reloading currently the stencil compiler has to be modified slightly.

Run: `node node_modules/stencil-plugin-tss/rewriteStencil.js`

This will do the necessary modifications. I will open a feature-request in the official Stencil project to make this configurable.

## Related

- [Stencil](https://stenciljs.com/)
- [Stencil Worldwide Slack](https://stencil-worldwide.slack.com)
- [Ionic Components](https://www.npmjs.com/package/@ionic/core)
- [Ionicons](http://ionicons.com/)
