### v2.0.1 (30.01.2020)

**NON-BREAKING CHANGES**

-   **fix**: Fix main entry point.
-   **fix**: Fix usePlugin function.
-   **chore**: Add rewriteStencil.js and styled.ts to dist again.

### v2.0.0 (30.01.2020)

**BREAKING CHANGES**

-   **refactor**: Did a complete rework on how this plugin operates. Will now start compiling styles when it is
initialized and only apply the already compiled styles in the transform hook.
-   **feat**: There are several new config options. See readme.md for details. 

### v1.2.0 (29.01.2020)

**FEATURES**

-   **feat**: Performance significantly improved by using @rollup/plugin-sucrase and heavy treeshaking

### v1.1.1 (28.01.2020)

**NON-BREAKING CHANGES**

-   **fix**: Fixed rollup plugin order

### v1.1.0 (28.01.2020)

**FEATURES**

- **feat**: Made rewriteStencil future save for stencil ^1.9.0
- **feat**: Added rollup-plugin-node-resolve and preserveSymlinks: true

### v1.0.2 (02.09.2019)

**NON-BREAKING CHANGES**

-   **chore**: Updated dependencies because of security alerts: set-value, mixin-deep, lodash, js-yaml.
